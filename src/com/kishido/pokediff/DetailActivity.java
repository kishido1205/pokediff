package com.kishido.pokediff;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kishido.pokediff.db.DBDataSource;
import com.kishido.pokediff.db.Pokemon;

public class DetailActivity extends ActionBarActivity implements OnClickListener {

	private static final String PREFS_DETAIL = "detail_prefs";
	private static final int REQUEST_SHOW_GUIDE = 1;
	public static final String KEY_EXTRA_POKEMON_1 = "pokemon_1";
	public static final String KEY_EXTRA_POKEMON_2 = "pokemon_2";

	private DBDataSource source;
	
	private View tab1, tab2, tab3;
	private View tabInd1, tabInd2, tabInd3;
	
	private StatHighlighter highlighter;
	
	public static int tabIndex = R.id.tabBasicInfo;
	
	private static FormIndex form1, form2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_detail);
		
		source = new DBDataSource(this);
		source.open();
		
		initializeView();
		
		try {
			String pokemon1 = getIntent().getStringExtra(KEY_EXTRA_POKEMON_1);
			String pokemon2 = getIntent().getStringExtra(KEY_EXTRA_POKEMON_2);
			loadDetails(pokemon1, pokemon2);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		onClick(findViewById(tabIndex));
	}

	private void initializeView() {
		Button tabBtn1 = (Button) findViewById(R.id.tabBasicInfo);
		Button tabBtn2 = (Button) findViewById(R.id.tabBreeding);
		Button tabBtn3 = (Button) findViewById(R.id.tabStats);
		
		tabInd1 = findViewById(R.id.tabIndicator1);
		tabInd2 = findViewById(R.id.tabIndicator2);
		tabInd3 = findViewById(R.id.tabIndicator3);

		tab1 = findViewById(R.id.tabContentBasic);
		tab2 = findViewById(R.id.tabContentBreeding);
		tab3 = findViewById(R.id.tabContentStat);
		
		tabBtn1.setOnClickListener(this);
		tabBtn2.setOnClickListener(this);
		tabBtn3.setOnClickListener(this);
		
		highlighter = new StatHighlighter();
	}

	private void loadDetails(String pokemon1, String pokemon2) throws IOException {
		
		List<Pokemon> detail1 = source.getPokemonByName(pokemon1);
		List<Pokemon> detail2 = source.getPokemonByName(pokemon2);
		
		ViewGroup defaultGroup1 = (ViewGroup) findViewById(R.id.defaultInfo1);
		ViewGroup defaultGroup2 = (ViewGroup) findViewById(R.id.defaultInfo2);

		ViewGroup contentTabBasic = (ViewGroup) findViewById(R.id.tabContentBasic);
		ViewGroup basicGroup1 = (ViewGroup) contentTabBasic.findViewById(R.id.pokemon1Stats);
		ViewGroup basicGroup2 = (ViewGroup) contentTabBasic.findViewById(R.id.pokemon2Stats);

		ViewGroup contentTabBreed = (ViewGroup) findViewById(R.id.tabContentBreeding);
		ViewGroup breedGroup1 = (ViewGroup) contentTabBreed.findViewById(R.id.pokemon1Stats);
		ViewGroup breedGroup2 = (ViewGroup) contentTabBreed.findViewById(R.id.pokemon2Stats);
		
		ViewGroup contentTabStat = (ViewGroup) findViewById(R.id.tabContentStat);
		ViewGroup statGroup1 = (ViewGroup) contentTabStat.findViewById(R.id.pokemon1Stats);
		ViewGroup statGroup2 = (ViewGroup) contentTabStat.findViewById(R.id.pokemon2Stats);
		
		int index1, index2;
		
		index1 = form1 == null ? 0 : form1.name.compareTo(detail1.get(0).name) == 0 ? form1.index : 0;
		index2 = form2 == null ? 0 : form2.name.compareTo(detail2.get(0).name) == 0 ? form2.index : 0;
		
		form1 = form1 == null ? new FormIndex() : form1;
		form2 = form2 == null ? new FormIndex() : form2;
		
		setPokemonDetail(defaultGroup1, basicGroup1, breedGroup1, statGroup1, 1, detail1, index1);
		setPokemonDetail(defaultGroup2, basicGroup2, breedGroup2, statGroup2, 2, detail2, index2);
		
		if (detail1.size() > 1 || detail2.size() > 2) {
			showGuideForMultiple();
			
		}
	}
	
	private void setPokemonDetail(ViewGroup defaultInfo, ViewGroup tabBasic, ViewGroup tabBreed, ViewGroup tabStat, int mode, List<Pokemon> details, int index) {
		Pokemon pokemon = details.get(index);
		
		PokemonDetailHolder holder = new PokemonDetailHolder();
		holder.defaultInfo = defaultInfo;
		holder.tabBasic = tabBasic;
		holder.tabBreed = tabBreed;
		holder.tabStat = tabStat;
		holder.mode = mode;
		holder.details = details;
		holder.index = index;
		
		// set default info
		TextView name = (TextView) defaultInfo.findViewById(R.id.textName);
		ImageView sprite = (ImageView) defaultInfo.findViewById(R.id.imageSprite);
		ImageView type1 = (ImageView) defaultInfo.findViewById(R.id.type1);
		ImageView type2 = (ImageView) defaultInfo.findViewById(R.id.type2);
		
		if (details.size() > 1) {
			defaultInfo.findViewById(R.id.imageTap).setVisibility(View.VISIBLE);
		}
		
		name.setText(getFormattedName(pokemon));
		
		String modelLocation = getModelLocation(pokemon);
		
		try {
			Drawable spriteDrawable = Drawable.createFromStream(getAssets().open(modelLocation), null);
			sprite.setImageDrawable(spriteDrawable);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		sprite.setTag(holder);
		sprite.setOnClickListener(this);
		
		type2.setVisibility(View.GONE);
		
		try {
			Drawable typeDrawable1 = Drawable.createFromStream(getAssets().open("type/" + pokemon.type1.toLowerCase(Locale.US) + ".gif"), null);
			type1.setImageDrawable(typeDrawable1);

			if (pokemon.type2.compareTo("--") != 0) {
				Drawable typeDrawable2 = Drawable.createFromStream(getAssets().open("type/" + pokemon.type2.toLowerCase(Locale.US) + ".gif"), null);
				type2.setImageDrawable(typeDrawable2);
				type2.setVisibility(View.VISIBLE);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// basic info
		TextView abilities = (TextView) tabBasic.findViewById(R.id.valueAbilities);
		TextView happiness = (TextView) tabBasic.findViewById(R.id.valueHappiness);
		TextView height = (TextView) tabBasic.findViewById(R.id.valueHeight);
		TextView weight = (TextView) tabBasic.findViewById(R.id.valueWeight);
		
		String ability = pokemon.ability1;
		if (pokemon.ability2.compareTo("--") != 0) {
			ability += "\n" + pokemon.ability2;
		}
		if (pokemon.hiddenAbility.compareTo("--") != 0) {
			ability += "\n" + pokemon.hiddenAbility;
			SpannableString spannedAbility = new SpannableString(ability);
			spannedAbility.setSpan(new StyleSpan(Typeface.ITALIC), ability.indexOf(pokemon.hiddenAbility), ability.length(), 0);
			abilities.setText(spannedAbility);
		} else {
			abilities.setText(ability);
		}
		
		happiness.setText(String.valueOf(pokemon.happiness));
		height.setText(pokemon.height + " m");
		weight.setText(pokemon.weight + " kg");
		
		// breeding
		TextView genderRatio = (TextView) tabBreed.findViewById(R.id.valueGenderRatio);
		TextView eggGroup = (TextView) tabBreed.findViewById(R.id.valueEggGroup);
		TextView eggCycle = (TextView) tabBreed.findViewById(R.id.valueEggCycle);
		TextView catchRate = (TextView) tabBreed.findViewById(R.id.valueCatchRate);
		TextView expGrowth = (TextView) tabBreed.findViewById(R.id.valueExpGrowth);
		
		String ratio = pokemon.genderRatio;
		if (ratio.contains(" to ")) {
			ratio += " (F to M)";
		}
		genderRatio.setText(ratio);
		
		String group = pokemon.egg1;
		if (pokemon.egg2.compareTo("--") != 0) {
			group += "\n" + pokemon.egg2;
		}
		eggGroup.setText(group);
		eggCycle.setText(String.valueOf(pokemon.eggStep));
		catchRate.setText(String.valueOf(pokemon.catchRate));
		expGrowth.setText(pokemon.expGrowth);
		
		// stats
		TextView hp = (TextView) tabStat.findViewById(R.id.valueHP);
		TextView atk = (TextView) tabStat.findViewById(R.id.valueATK);
		TextView def = (TextView) tabStat.findViewById(R.id.valueDEF);
		TextView spatk = (TextView) tabStat.findViewById(R.id.valueSPATK);
		TextView spdef = (TextView) tabStat.findViewById(R.id.valueSPDEF);
		TextView speed = (TextView) tabStat.findViewById(R.id.valueSpeed);
		
		hp.setText(String.valueOf(pokemon.hp));
		atk.setText(String.valueOf(pokemon.atk));
		def.setText(String.valueOf(pokemon.def));
		spatk.setText(String.valueOf(pokemon.spatk));
		spdef.setText(String.valueOf(pokemon.spdef));
		speed.setText(String.valueOf(pokemon.speed));
		
		highlighter.set(tabStat, pokemon);
	}

	private void showGuideForMultiple() {
		if (!getSharedPreferences(PREFS_DETAIL, Context.MODE_PRIVATE).getBoolean(GuideActivity.KEY_EXTRA_SHOW_AGAIN, true)) {
			return;
		}
		
		Intent intent = new Intent(this, GuideActivity.class);
		intent.putExtra(GuideActivity.KEY_EXTRA_MESSAGE, "Pokemon with a 'Ribbon' icon on the upper left corner of their model has multiple forms. Tap the image to change form.");
		startActivityForResult(intent, REQUEST_SHOW_GUIDE);
	}

	private String getFormattedName(Pokemon pokemon) {
		String name = pokemon.name;
		String form = pokemon.form;
		
		if (form.compareTo("--") != 0) {
			if (form.contains("Mega")) {
				if (form.contains("X") || form.contains("Y")) {
					String[] split = form.split(" ");
					return split[0] + " " + name + " " + split[1];
				}
			}
			
			return form + " " + name;
		}
		
		return name;
	}

	private String getModelLocation(Pokemon pokemon) {
		String path = "model/";
		String formattedNumber = String.format(Locale.US, "%03d", pokemon.num);
		
		if (pokemon.form.compareTo("--") != 0) {
			String form = pokemon.form.toLowerCase(Locale.US);
			form = form.replace(" ", "-");
			
			formattedNumber += "-" + form;
		}
		
		return path + formattedNumber + ".png";
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		switch (id) {
		case R.id.tabBasicInfo:
			tabInd1.setVisibility(View.VISIBLE);
			tabInd2.setVisibility(View.INVISIBLE);
			tabInd3.setVisibility(View.INVISIBLE);

			tab1.setVisibility(View.VISIBLE);
			tab2.setVisibility(View.GONE);
			tab3.setVisibility(View.GONE);
			
			tabIndex = id;
			break;
		case R.id.tabBreeding:
			tabInd1.setVisibility(View.INVISIBLE);
			tabInd2.setVisibility(View.VISIBLE);
			tabInd3.setVisibility(View.INVISIBLE);

			tab1.setVisibility(View.GONE);
			tab2.setVisibility(View.VISIBLE);
			tab3.setVisibility(View.GONE);
			
			tabIndex = id;
			break;
		case R.id.tabStats:
			tabInd1.setVisibility(View.INVISIBLE);
			tabInd2.setVisibility(View.INVISIBLE);
			tabInd3.setVisibility(View.VISIBLE);

			tab1.setVisibility(View.GONE);
			tab2.setVisibility(View.GONE);
			tab3.setVisibility(View.VISIBLE);
			
			tabIndex = id;
			break;
		case R.id.imageSprite:
			PokemonDetailHolder holder = (PokemonDetailHolder) v.getTag();
			int max = holder.details.size();
			int index = holder.index + 1;
			if (index >= max) {
				index = 0;
			}
			
			if (holder.mode == 1) {
				form1.index = index;
				form1.name = holder.details.get(index).name;
			} else {
				form2.index = index;
				form2.name = holder.details.get(index).name;
			}
			
			setPokemonDetail(holder.defaultInfo, holder.tabBasic, holder.tabBreed, holder.tabStat, holder.mode, holder.details, index);
		}
	}

	private static class PokemonDetailHolder {
		ViewGroup defaultInfo;
		ViewGroup tabBasic, tabBreed, tabStat;
		int mode;
		List<Pokemon> details;
		int index;
	}
	
	private static class StatHighlighter {
		ViewGroup stat1, stat2;
		Pokemon pokemon1, pokemon2;
		
		public void set(ViewGroup stat, Pokemon pokemon) {
			int id1 = stat1 == null ? -1 : stat1.getId();
			int id2 = stat2 == null ? -1 : stat2.getId();
			int id = stat.getId();
			
			if (stat1 == null && stat2 == null) {
				stat1 = stat;
				pokemon1 = pokemon;
			} else if (stat1 != null && stat2 != null) {
				if (id1 == id) {
					stat1 = stat;
					pokemon1 = pokemon;
				} else {
					stat2 = stat;
					pokemon2 = pokemon;
				}
			} else {
				if (stat1 == null) {
					if (id2 == id) {
						stat2 = stat;
						pokemon2 = pokemon;
					} else {
						stat1 = stat;
						pokemon1 = pokemon;
					}
				} else {
					if (id1 == id) {
						stat1 = stat;
						pokemon1 = pokemon;
					} else {
						stat2 = stat;
						pokemon2 = pokemon;
					}
				}
			}
			
			doHighlight();
		}

		private void doHighlight() {
			if (stat1 == null || stat2 == null) {
				return;
			}
			
			processHighlight(R.id.valueHP, pokemon1.hp, pokemon2.hp);
			processHighlight(R.id.valueATK, pokemon1.atk, pokemon2.atk);
			processHighlight(R.id.valueDEF, pokemon1.def, pokemon2.def);
			processHighlight(R.id.valueSPATK, pokemon1.spatk, pokemon2.spatk);
			processHighlight(R.id.valueSPDEF, pokemon1.spdef, pokemon2.spdef);
			processHighlight(R.id.valueSpeed, pokemon1.speed, pokemon2.speed);
		}
		
		private void processHighlight(int id, int stats1, int stats2) {
			TextView val1 = (TextView) stat1.findViewById(id);
			TextView val2 = (TextView) stat2.findViewById(id);
			
			val1.setTextColor(Color.BLACK);
			val2.setTextColor(Color.BLACK);
			
			if (stats1 == stats2) {
				// do nothing
			} else if (stats1 > stats2) {
				val2.setTextColor(Color.LTGRAY);
			} else {
				val1.setTextColor(Color.LTGRAY);
			}
		}
	}
	
	private class FormIndex {	
		String name = "egg";
		int index = 0;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_SHOW_GUIDE) {
			boolean showAgain = data.getBooleanExtra(GuideActivity.KEY_EXTRA_SHOW_AGAIN, true);
			SharedPreferences prefs = getSharedPreferences(PREFS_DETAIL, Context.MODE_PRIVATE);
			prefs.edit().putBoolean(GuideActivity.KEY_EXTRA_SHOW_AGAIN, showAgain).commit();
		}
	}
}
