package com.kishido.pokediff.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kishido.pokediff.R;

public class AutoCompleteAdapter extends ArrayAdapter<String> {

	protected List<String> names;
	protected Context context;
	protected LayoutInflater inflater;
	protected AssetManager assets;
	
	public AutoCompleteAdapter(Context context, List<String> names) {
		super(context, R.layout.part_autocomplete, names);
		
		this.context = context;
		this.assets = context.getAssets();
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.names = names;
	}
	
	@SuppressLint("InflateParams")
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.part_autocomplete, null);
			
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView.findViewById(R.id.icon);
			holder.name = (TextView) convertView.findViewById(R.id.name);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		String name = getItem(position);
		int index = names.indexOf(name);
		
		holder.name.setText(name);
		holder.icon.setImageDrawable(loadDrawable(index+1));
		
		return convertView;
	}
	
	private Drawable loadDrawable(int position) {
		try {
			InputStream is = assets.open("icon/" + position + ".png");
			Drawable d = Drawable.createFromStream(is, null);
			return d;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	static class ViewHolder {
		ImageView icon;
		TextView name;
	}
}
