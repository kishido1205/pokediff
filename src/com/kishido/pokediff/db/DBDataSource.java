package com.kishido.pokediff.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DBDataSource {

	private DBHelper dbHelper;
	private SQLiteDatabase db;
	
	public DBDataSource(Context context) {
		dbHelper = new DBHelper(context);
	}
	
	public void open() throws SQLiteException {
		db = dbHelper.getReadableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public List<Pokemon> getAllPokemon() {
		List<Pokemon> list = new ArrayList<Pokemon>();
		
		Cursor cursor = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_POKEMON, null);
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			Pokemon pkmn = cursorToPkmn(cursor);
			list.add(pkmn);
			cursor.moveToNext();
		}
		
		return list;
	}
	
	public List<String> getAllPokemonName() {
	    List<String> list = new ArrayList<String>();
	    
	    Cursor cursor = db.rawQuery("SELECT DISTINCT " + DBHelper.COLUMN_NAME + " FROM " + DBHelper.TABLE_POKEMON + ";", null);
	    cursor.moveToFirst();
	    
	    while (!cursor.isAfterLast()) {
	        String name = cursor.getString(0);
	        list.add(name);
	        cursor.moveToNext();
	    }
	    
	    return list;
	}
	
	public List<Pokemon> getPokemonByName(String name) {
		List<Pokemon> list = new ArrayList<Pokemon>();
		
		Cursor cursor = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_POKEMON + " WHERE " + DBHelper.COLUMN_NAME + "=?", new String[] { name });
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			Pokemon pkmn = cursorToPkmn(cursor);
			list.add(pkmn);
			cursor.moveToNext();
		}
		
		return list;
	}
	
	public void insertPokemon(Pokemon pkmn) {
	    ContentValues cv = new ContentValues();
	    
	    cv.put(DBHelper.COLUMN_NUM, pkmn.num);
	    cv.put(DBHelper.COLUMN_NAME, pkmn.name);
	    cv.put(DBHelper.COLUMN_FORM, pkmn.form);
	    cv.put(DBHelper.COLUMN_TYPE_1, pkmn.type1);
	    cv.put(DBHelper.COLUMN_TYPE_2, pkmn.type2);
	    cv.put(DBHelper.COLUMN_ABILITY_1, pkmn.ability1);
	    cv.put(DBHelper.COLUMN_ABILITY_2, pkmn.ability2);
	    cv.put(DBHelper.COLUMN_HIDDEN_ABILITY, pkmn.hiddenAbility);
	    cv.put(DBHelper.COLUMN_GENDER_RATIO, pkmn.genderRatio);
	    cv.put(DBHelper.COLUMN_CATCH_RATE, pkmn.catchRate);
	    cv.put(DBHelper.COLUMN_EGG_GROUP_1, pkmn.egg1);
	    cv.put(DBHelper.COLUMN_EGG_GROUP_2, pkmn.egg2);
	    cv.put(DBHelper.COLUMN_EGG_STEP, pkmn.eggStep);
	    cv.put(DBHelper.COLUMN_HEIGHT, pkmn.height);
	    cv.put(DBHelper.COLUMN_WEIGHT, pkmn.weight);
	    cv.put(DBHelper.COLUMN_EXP_GROWTH, pkmn.expGrowth);
	    cv.put(DBHelper.COLUMN_HAPPINESS, pkmn.happiness);
	    cv.put(DBHelper.COLUMN_HP, pkmn.hp);
	    cv.put(DBHelper.COLUMN_ATK, pkmn.atk);
	    cv.put(DBHelper.COLUMN_DEF, pkmn.def);
	    cv.put(DBHelper.COLUMN_SPATK, pkmn.spatk);
	    cv.put(DBHelper.COLUMN_SPDEF, pkmn.spdef);
	    cv.put(DBHelper.COLUMN_SPEED, pkmn.speed);
	    
	    db.insert(DBHelper.TABLE_POKEMON, null, cv);
	}

	private Pokemon cursorToPkmn(Cursor cursor) {
		Pokemon pkmn = new Pokemon();
		
		pkmn.num = cursor.getInt(1);
		pkmn.name = cursor.getString(2);
		pkmn.form = cursor.getString(3);
		pkmn.type1 = cursor.getString(4);
		pkmn.type2 = cursor.getString(5);
		pkmn.ability1 = cursor.getString(6);
		pkmn.ability2 = cursor.getString(7);
		pkmn.hiddenAbility = cursor.getString(8);
		pkmn.genderRatio = cursor.getString(9);
		pkmn.catchRate = cursor.getInt(10);
		pkmn.egg1 = cursor.getString(11);
		pkmn.egg2 = cursor.getString(12);
		pkmn.eggStep = cursor.getInt(13);
		pkmn.height = cursor.getFloat(14);
		pkmn.weight = cursor.getFloat(15);
		pkmn.expGrowth = cursor.getString(16);
		pkmn.happiness = cursor.getInt(17);
		pkmn.hp = cursor.getInt(18);
		pkmn.atk = cursor.getInt(19);
		pkmn.def = cursor.getInt(20);
		pkmn.spatk = cursor.getInt(21);
		pkmn.spdef = cursor.getInt(22);
		pkmn.speed = cursor.getInt(23);
		
		return pkmn;
	}
}
