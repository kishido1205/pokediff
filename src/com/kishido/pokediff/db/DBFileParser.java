package com.kishido.pokediff.db;

import java.io.IOException;
import java.io.InputStream;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;

public class DBFileParser {

    public static final int JXL_VERSION = 1;

    private boolean shouldLoad = true;

    private Context context;
    
    private FileLoaderTask task = new FileLoaderTask() {
		
		@Override
		public void onTaskSize(int size) {
			// default task, do nothing
		}
		
		@Override
		public void onTaskIterate(int progress) {
			// default task, do nothing
		}

		@Override
		public void onTaskComplete() {
			// default task, do nothing
		}
	};

    public DBFileParser(Context context) {
        this.context = context;

        SharedPreferences prefs = context.getSharedPreferences("pkmn_meta",
                Context.MODE_PRIVATE);
        int versionLoaded = prefs.getInt("jxl_version", -1);
        if (versionLoaded == JXL_VERSION) {
            shouldLoad = false;
        }
    }
    
    public void setLoaderTask(FileLoaderTask task) {
    	this.task = task;
    }

    public void loadDatabase(AssetManager assets) throws IOException,
            BiffException {
        if (shouldLoad) {
            InputStream is = assets.open("data/pkmn.xls");
            Workbook wb = Workbook.getWorkbook(is);
            makeDatabase(wb);
        }
        task.onTaskComplete();
    }

    private void makeDatabase(Workbook wb) {
        DBDataSource db = new DBDataSource(context);
        db.open();

        Sheet pokemonSheet = wb.getSheet("Pokemon");

        int row = pokemonSheet.getRows();
        task.onTaskSize(row);

        for (int i = 1; i < row; i++) {
            db.insertPokemon(rowToPokemon(pokemonSheet, i));
            task.onTaskIterate(i);
        }

        SharedPreferences prefs = context.getSharedPreferences("pkmn_meta",
                Context.MODE_PRIVATE);
        prefs.edit().putInt("jxl_version", JXL_VERSION).commit();

        db.close();
    }

    private Pokemon rowToPokemon(Sheet pokemonSheet, int i) {
        Pokemon pkmn = new Pokemon();

        pkmn.num = Integer.valueOf(pokemonSheet.getCell(0, i).getContents());
        pkmn.name = pokemonSheet.getCell(1, i).getContents();
        pkmn.form = pokemonSheet.getCell(2, i).getContents();
        pkmn.type1 = pokemonSheet.getCell(3, i).getContents();
        pkmn.type2 = pokemonSheet.getCell(4, i).getContents();
        pkmn.ability1 = pokemonSheet.getCell(5, i).getContents();
        pkmn.ability2 = pokemonSheet.getCell(6, i).getContents();
        pkmn.hiddenAbility = pokemonSheet.getCell(7, i).getContents();
        pkmn.genderRatio = pokemonSheet.getCell(8, i).getContents();
        pkmn.catchRate = Integer.valueOf(pokemonSheet.getCell(9, i).getContents());
        pkmn.egg1 = pokemonSheet.getCell(10, i).getContents();
        pkmn.egg2 = pokemonSheet.getCell(11, i).getContents();
        pkmn.eggStep = Integer.valueOf(pokemonSheet.getCell(12, i).getContents());
        pkmn.height = Float.valueOf(pokemonSheet.getCell(13, i).getContents());
        pkmn.weight = Float.valueOf(pokemonSheet.getCell(14, i).getContents());
        pkmn.expGrowth = pokemonSheet.getCell(15, i).getContents();
        pkmn.happiness = Integer.valueOf(pokemonSheet.getCell(16, i).getContents());
        pkmn.hp = Integer.valueOf(pokemonSheet.getCell(17, i).getContents());
        pkmn.atk = Integer.valueOf(pokemonSheet.getCell(18, i).getContents());
        pkmn.def = Integer.valueOf(pokemonSheet.getCell(19, i).getContents());
        pkmn.spatk = Integer.valueOf(pokemonSheet.getCell(20, i).getContents());
        pkmn.spdef = Integer.valueOf(pokemonSheet.getCell(21, i).getContents());
        pkmn.speed = Integer.valueOf(pokemonSheet.getCell(22, i).getContents());
        
        return pkmn;
    }
    
    public interface FileLoaderTask {
    	
    	public void onTaskSize(int size);
    	
    	public void onTaskIterate(int progress);
    	
    	public void onTaskComplete();
    }
}
