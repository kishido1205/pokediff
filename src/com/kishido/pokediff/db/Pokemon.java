package com.kishido.pokediff.db;

public class Pokemon {

	public int num;
	public String name;
	public String form;
	public String type1, type2;
	public String ability1, ability2, hiddenAbility;
	public String genderRatio;
	public int catchRate;
	public String egg1, egg2;
	public int eggStep;
	public float height, weight;
	public String expGrowth;
	public int happiness;
	public int hp, atk, def, spatk, spdef, speed;
}
