package com.kishido.pokediff.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	public static final String DB_PKMNCOMPARE = "pkmn_compare";
	public static final int DB_VERSION = 1;

	public static final String TABLE_POKEMON = "pkmn";
	
	public static final String COLUMN_ID = "id";
	
	public static final String COLUMN_NUM = "num";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_FORM = "form";
	public static final String COLUMN_TYPE_1 = "type_1";
	public static final String COLUMN_TYPE_2 = "type_2";
	public static final String COLUMN_ABILITY_1 = "ability_1";
	public static final String COLUMN_ABILITY_2 = "ability_2";
	public static final String COLUMN_HIDDEN_ABILITY = "hidden_ability";
	public static final String COLUMN_GENDER_RATIO = "gender_ratio";
	public static final String COLUMN_CATCH_RATE = "catch_rate";
	public static final String COLUMN_EGG_GROUP_1 = "egg_1";
	public static final String COLUMN_EGG_GROUP_2 = "egg_2";
	public static final String COLUMN_EGG_STEP = "egg_step";
	public static final String COLUMN_HEIGHT = "height";
	public static final String COLUMN_WEIGHT = "weight";
	public static final String COLUMN_EXP_GROWTH = "exp_growth";
	public static final String COLUMN_HAPPINESS = "happiness";
	public static final String COLUMN_HP = "hp";
	public static final String COLUMN_ATK = "atk";
	public static final String COLUMN_DEF = "def";
	public static final String COLUMN_SPATK = "spatk";
	public static final String COLUMN_SPDEF = "spdef";
	public static final String COLUMN_SPEED = "speed";
	
	public static final int NUM_COLUMN_PKMN = 23;
	
	public static final String CREATE_TABLE_POKEMON = "CREATE TABLE "
			+ TABLE_POKEMON + " ("
			+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_NUM + " INTEGER, "
			+ COLUMN_NAME + " TEXT, "
			+ COLUMN_FORM + " TEXT, "
			+ COLUMN_TYPE_1 + " TEXT, "
			+ COLUMN_TYPE_2 + " TEXT, "
			+ COLUMN_ABILITY_1 + " TEXT, "
			+ COLUMN_ABILITY_2 + " TEXT, "
			+ COLUMN_HIDDEN_ABILITY + " TEXT, "
			+ COLUMN_GENDER_RATIO + " TEXT, "
			+ COLUMN_CATCH_RATE + " INTEGER, "
			+ COLUMN_EGG_GROUP_1 + " TEXT, "
			+ COLUMN_EGG_GROUP_2 + " TEXT, "
			+ COLUMN_EGG_STEP + " INTEGER, "
			+ COLUMN_HEIGHT + " REAL, "
			+ COLUMN_WEIGHT + " REAL, "
			+ COLUMN_EXP_GROWTH + " TEXT, "
			+ COLUMN_HAPPINESS + " INTEGER, "
			+ COLUMN_HP + " INTEGER, "
			+ COLUMN_ATK + " INTEGER, "
			+ COLUMN_DEF + " INTEGER, "
			+ COLUMN_SPATK + " INTEGER, "
			+ COLUMN_SPDEF + " INTEGER, "
			+ COLUMN_SPEED + " INTEGER);";
	
	public DBHelper(Context context) {
		super(context, DB_PKMNCOMPARE, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_POKEMON);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POKEMON);
		onCreate(db);
	}

}
