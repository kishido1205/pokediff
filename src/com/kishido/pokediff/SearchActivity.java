package com.kishido.pokediff;

import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;

import com.kishido.pokediff.db.AutoCompleteAdapter;
import com.kishido.pokediff.db.DBDataSource;
import com.larvalabs.svgandroid.SVGParser;

public class SearchActivity extends ActionBarActivity implements OnClickListener {

	private DBDataSource source;
	
	private List<String> pokemonNameList;
	
	private Animation shakeAnimation;
	
	private AutoCompleteTextView input1, input2;
	private Button btnCompare;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
        setContentView(R.layout.activity_search);

        ImageView image = (ImageView) findViewById(R.id.imgMascot);
        image.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        image.setImageDrawable(SVGParser.getSVGFromResource(getResources(), R.raw.img_prof).createPictureDrawable());
     
        source = new DBDataSource(this);
        source.open();
        
        initializeView();
    }

    private void initializeView() {
		input1 = (AutoCompleteTextView) findViewById(R.id.inputPokemon1);
		input2 = (AutoCompleteTextView) findViewById(R.id.inputPokemon2);
		btnCompare = (Button) findViewById(R.id.btnCompare);
		
		pokemonNameList = source.getAllPokemonName();
		AutoCompleteAdapter adapter = new AutoCompleteAdapter(this, pokemonNameList);
		input1.setAdapter(adapter);
		input2.setAdapter(adapter);
		
		btnCompare.setOnClickListener(this);
		
		shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
	}
    
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		switch (id) {
		case R.id.btnCompare:
			if (verifyInput()) {
				startDetailsActivity();
			}
			break;
		}
	}

	private void startDetailsActivity() {
		Intent intent = new Intent(this, DetailActivity.class);
		intent.putExtra(DetailActivity.KEY_EXTRA_POKEMON_1, input1.getEditableText().toString());
		intent.putExtra(DetailActivity.KEY_EXTRA_POKEMON_2, input2.getEditableText().toString());
		startActivity(intent);
	}

	private boolean verifyInput() {
		boolean clear = false;
		
		String pokemon1 = input1.getEditableText().toString();
		String pokemon2 = input2.getEditableText().toString();
		
		pokemon1 = formalizeInput(pokemon1);
		pokemon2 = formalizeInput(pokemon2);
		
		if (TextUtils.isEmpty(pokemon1) || !pokemonNameList.contains(pokemon1)) {
			input1.startAnimation(shakeAnimation);
			clear = false;
		} else {
			clear = true;
		}

		if (TextUtils.isEmpty(pokemon2) || !pokemonNameList.contains(pokemon2)) {
			input2.startAnimation(shakeAnimation);
			clear = false;
		} else {
			clear = true;
		}
		
		return clear;
	}
	
	private String formalizeInput(String name) {
		if (TextUtils.isEmpty(name)) {
			return name;
		}
		int len = name.length();
		
		if (len == 1) {
			return name.toUpperCase(Locale.US);
		}
		
		name = name.toLowerCase(Locale.US);
		
		String firstLetter = String.valueOf(name.charAt(0)).toUpperCase(Locale.US);
		String otherLetters = name.substring(1);
		
		return firstLetter + otherLetters;
	}
}
