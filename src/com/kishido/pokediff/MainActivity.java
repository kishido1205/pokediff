package com.kishido.pokediff;

import java.io.IOException;

import jxl.read.biff.BiffException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.kishido.pokediff.db.DBFileParser;
import com.kishido.pokediff.db.DBFileParser.FileLoaderTask;

public class MainActivity extends Activity implements Handler.Callback,
		FileLoaderTask {

	private static final int DELAY_SEND_MESSAGE = 500;
	private static final int MESSAGE_LOAD_DB = 1;
	private Handler handler;

	private ProgressBar taskProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);

		handler = new Handler(this);
		handler.sendEmptyMessageDelayed(MESSAGE_LOAD_DB, DELAY_SEND_MESSAGE);
	}

	@Override
	public boolean handleMessage(Message msg) {
		taskProgress = (ProgressBar) findViewById(R.id.progressTask);
		
		final DBFileParser parser = new DBFileParser(this);
		parser.setLoaderTask(this);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					parser.loadDatabase(getAssets());
				} catch (BiffException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
		return true;
	}

	@Override
	public void onBackPressed() {

	}

	private void startSearchActivity() {
		startActivity(new Intent(this, SearchActivity.class));
		finish();
	}

	@Override
	public void onTaskSize(int size) {
		taskProgress.setMax(size);
		taskProgress.setProgress(0);
	}

	@Override
	public void onTaskIterate(int progress) {
		taskProgress.setProgress(progress + 1);
	}

	@Override
	public void onTaskComplete() {
		taskProgress.setProgress(taskProgress.getMax());
		startSearchActivity();
	}
}
