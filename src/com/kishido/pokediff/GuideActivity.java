package com.kishido.pokediff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVGParser;

public class GuideActivity extends Activity {

	public static final String KEY_EXTRA_SHOW_AGAIN = "show_again";
	public static final String KEY_EXTRA_MESSAGE = "message";

	private boolean showAgain = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_guide);
		
		String message = getIntent().getStringExtra(KEY_EXTRA_MESSAGE);

        ImageView image = (ImageView) findViewById(R.id.imageGuide);
        image.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        image.setImageDrawable(SVGParser.getSVGFromResource(getResources(), R.raw.img_prof).createPictureDrawable());
     
		((TextView) findViewById(R.id.textGuide)).setText(message);
		
		findViewById(R.id.buttonClose).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent data = new Intent();
				data.putExtra(KEY_EXTRA_SHOW_AGAIN, showAgain);
				setResult(RESULT_OK, data);
				finish();
			}
		});
		
		((CheckBox) findViewById(R.id.checkShowAgain)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				showAgain = !isChecked;
			}
		});
	}
}
